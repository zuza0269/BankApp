#!/bin/sh

python manage.py migrate --no-input
python manage.py collectstatic --no-input
py.test

gunicorn bank_project.wsgi:application --bind :8000

