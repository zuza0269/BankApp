from django.contrib.auth.models import User
from django.test import TestCase, Client
from django.urls import reverse
from django.contrib.auth.decorators import login_required
from bank_app.models import Customer, Rank, Account, Ledger
import json
import pytest


class ViewsTestCase(TestCase):

    @pytest.mark.django_db
    def setUp(self):
        self.client = Client()
        self.user = User.objects.create_user(username='A', email='a@a.com', password='aaa_123_aaa',is_staff=True)

    @pytest.mark.django_db
    def testLogin(self):
        self.client.login(username='A', password='aaa_123_aaa')
        response = self.client.get(reverse('login_app:login'))
        self.assertEqual(response.status_code, 200)

    @pytest.mark.django_db
    @login_required
    def test_staff_index_GET(self):
        response = self.client.get(reverse('bank_app:staff'))

        self.assertEquals(response.status_code, 302)
        # self.assertRedirects(response, 'bank_app/staff_index.html')
